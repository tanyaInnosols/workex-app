import { Component, Input } from '@angular/core';

@Component({
  selector: 'image-container',
  templateUrl: 'image.html'
})
export class ImageContainer {
  @Input() src: string
}
