import { NgModule } from '@angular/core';

import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { SettingsPage } from '../pages/settings/settings';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { FeedPage } from '../pages/feed/feed';
import { SchedulePage } from '../pages/schedule/schedule';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';
import { ActionsPage } from '../pages/actions/actions';
import { AttendancePage } from '../pages/attendance/attendance';
import { AttendanceSummaryPage } from '../pages/attendance/attendance-summary';
import { AttendanceDaywisePage } from '../pages/attendance/attendance-daywise';
import { QuizListPage } from '../pages/quiz/quiz';
import { QuizPage } from '../pages/quiz/que';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SalarySlipPage } from '../pages/salary-slip/salary-slip'
import { VisitStorePage } from '../pages/visit/visitstore'
import { VisitOptionsPage } from '../pages/visit/visitoptions'
import { VisitGsbPage } from '../pages/visit/visitgsb'
import { VisitVisibilityPage } from '../pages/visit/visitvisibility'
import { VisitCompetitionPage } from '../pages/visit/visitcompetition'
import { VisitStockPage } from '../pages/visit/visitstock'
import { VisitSubmitPage } from '../pages/visit/visitsubmit'
import { VisitSuccessPage } from '../pages/visit/visitsuccess'

import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { ImageContainer } from '../components/image-container/image'

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';


@NgModule({
  declarations: [
    ConferenceApp,
    SettingsPage,
    AccountPage,
    LoginPage,
    FeedPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    ActionsPage,
    AttendancePage,
    AttendanceSummaryPage,
    AttendanceDaywisePage,
    QuizListPage,
    QuizPage,
    SalarySlipPage,
    DashboardPage,
    VisitStorePage,
    VisitOptionsPage,
    VisitGsbPage,
    VisitVisibilityPage,
    VisitCompetitionPage,
    VisitStockPage,
    VisitSubmitPage,
    VisitSuccessPage,
    ProgressBarComponent,
    ImageContainer
  ],
  imports: [
    IonicModule.forRoot(ConferenceApp,{tabsPlacement: 'top'})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    SettingsPage,
    FeedPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    ActionsPage,
    SalarySlipPage,
    AttendancePage,
    AttendanceSummaryPage,
    AttendanceDaywisePage,
    QuizListPage,
    QuizPage,
    DashboardPage,
    VisitStorePage,
    VisitOptionsPage,
    VisitGsbPage,
    VisitVisibilityPage,
    VisitCompetitionPage,
    VisitStockPage,
    VisitSubmitPage,
    VisitSuccessPage
  ],
  providers: [ConferenceData, UserData, Storage]
})
export class AppModule { }
