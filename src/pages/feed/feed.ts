import { Component } from '@angular/core';

import { ActionSheet, ActionSheetController, App, Config, NavController } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';
import { AttendancePage } from '../attendance/attendance';
import { ConferenceData } from '../../providers/conference-data';
import { QuizListPage } from '../quiz/quiz';
import { SalarySlipPage } from '../salary-slip/salary-slip';

@Component({
  selector: 'page-feeds',
  templateUrl: 'feed.html'
})
export class FeedPage {
  actionSheet: ActionSheet;
  speakers: any[] = [];
  feeds: any[] = [];

  constructor(public actionSheetCtrl: ActionSheetController, public app: App,
  public navCtrl: NavController, public confData: ConferenceData, public config: Config) {}
  check_in() {
    const root = this.app.getRootNav();
    root.push(AttendancePage);
    //this.navCtrl.push(AttendancePage);
  }
  goto_quiz(){
    const root = this.app.getRootNav();
    root.push(QuizListPage);
  }
  download_salaryslip(){
    const root = this.app.getRootNav();
    root.push(SalarySlipPage);
  }
  ionViewDidLoad() {
    this.confData.getSpeakers().subscribe((speakers: any[]) => {
      this.speakers = speakers;
    });
    this.feeds = [
      {
        icon: 'calendar',
        desc: 'Your attendance is pending!',
        need_btn: function(){return true},
        btn_text: 'Check In Now',
        btn_fnc: this.check_in.bind(this)
      },
      {
        icon: 'help-circle',
        desc: 'New quiz for you!',
        need_btn: function(){return true},
        btn_text: 'Attend Now',
        btn_fnc: this.goto_quiz.bind(this)
      },
      {
        icon: 'ribbon',
        desc: 'Ramesh topped yesterday quiz @ 9/10',
        congratulated: false,
        need_btn: function(feed: any){
          return !feed.congratulated;
        },
        congrats: 50,
        btn_text: 'Congratulate',
        btn_fnc: function(feed: any){
          feed.congrats = feed.congrats + 1;
          feed.congratulated = true;
        }
      },
      {
        icon: 'thumbs-up',
        desc: 'Congrats! You topped sale for last month across India',
        congrats: 20,
        need_btn: function(){return false},
        btn_text: 'Share News',
        btn_fnc: this.check_in.bind(this)
      },
      {
        icon: 'cash',
        desc: 'Rs. 13500 as Salary for Feb 2017 processed to your account XXXXXX8992.',
        btn_text: 'Salary Slip',
        need_btn: function(){return true},
        btn_fnc: this.download_salaryslip.bind(this)
      }
    ]
  }
}
