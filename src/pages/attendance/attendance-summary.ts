import { Component } from '@angular/core';
import { App, ActionSheet, ActionSheetController, Config, NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { AttendanceDaywisePage } from '../attendance/attendance-daywise';

@Component({
  selector: 'page-user',
  templateUrl: 'attendance-summary.html'
})
export class AttendanceSummaryPage {

  constructor(
  public app: App,
  public navCtrl: NavController) {}

  ionViewDidLoad() {
  }

  daywise(){
    const root = this.app.getRootNav();
    root.push(AttendanceDaywisePage);
  }
}
