import { Component } from '@angular/core';
import { App, ActionSheet, ActionSheetController, Config, NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-user',
  templateUrl: 'attendance-daywise.html'
})
export class AttendanceDaywisePage {
  actionSheet: ActionSheet;
  data: any[] = [];
  constructor(public actionSheetCtrl: ActionSheetController,
  public app: App,
  public navCtrl: NavController, public config: Config) {}

  ionViewDidLoad() {
    this.data = [
      {
        name: "AiA",
        code: "AI101",
        limit: 25000,
        account: "Life Insurance"
      },
      {
        name: "Cargills",
        code: "CF001",
        limit: 30000,
        account: "Food City"
      }
    ]
  }

}
