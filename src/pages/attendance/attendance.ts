import { Component } from '@angular/core';
import { App, ActionSheet, ActionSheetController, Config, NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-user',
  templateUrl: 'attendance.html'
})
export class AttendancePage {
  actionSheet: ActionSheet;

  constructor(public actionSheetCtrl: ActionSheetController,
  public app: App,
  public navCtrl: NavController, public config: Config) {}

  ionViewDidLoad() {
  }

  submitAttendance(){
    const root = this.app.getRootNav();
    root.push(TabsPage, { tabIndex: 1 });
  }
}
