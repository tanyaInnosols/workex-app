import { Component } from '@angular/core';
import { App, ActionSheet, ActionSheetController, Config, NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { QuizPage } from '../quiz/que'

@Component({
  selector: 'page-user',
  templateUrl: 'quiz.html'
})
export class QuizListPage {
  actionSheet: ActionSheet;

  constructor(public actionSheetCtrl: ActionSheetController,
  public app: App,
  public navCtrl: NavController, public config: Config) {}

  ionViewDidLoad() {
  }
  attend_quiz(){
    const root = this.app.getRootNav();
    root.push(QuizPage);
  }
  gotohome(){
    const root = this.app.getRootNav();
    root.push(TabsPage, { tabIndex: 1 });
  }
}
