import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';

import { SettingsPage } from '../settings/settings';
import { SchedulePage } from '../schedule/schedule';
import { FeedPage } from '../feed/feed';
import { ActionsPage } from '../actions/actions';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // set the root pages for each tab
  tab1Root: any = FeedPage;
  tab2Root: any = ActionsPage;
  tab3Root: any = SchedulePage;
  tab4Root: any = SettingsPage;
  mySelectedIndex: number;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

}
