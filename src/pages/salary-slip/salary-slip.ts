import { Component, ViewChild } from '@angular/core';
import { App, ActionSheet, ActionSheetController, Config, NavController } from 'ionic-angular';

@Component({
  selector: 'page-user',
  templateUrl: 'salary-slip.html'
})
export class SalarySlipPage {
  actionSheet: ActionSheet;
  
  constructor(public actionSheetCtrl: ActionSheetController,
  public app: App,
  public navCtrl: NavController, public config: Config) {}
}
