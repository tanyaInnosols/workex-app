import { Component } from '@angular/core';
import { App, AlertController, NavController, ToastController } from 'ionic-angular';
import { AttendancePage } from '../attendance/attendance';
import { DashboardPage } from '../dashboard/dashboard';
import { QuizListPage } from '../quiz/quiz';
import { VisitStorePage } from '../visit/visitstore'
import { AttendanceSummaryPage } from '../attendance/attendance-summary';

@Component({
  selector: 'page-user',
  templateUrl: 'actions.html'
})
export class ActionsPage {

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public app: App
  ) {

  }

  ionViewDidEnter() {

  }

  markAttendance() {
    const root = this.app.getRootNav();
    root.push(AttendancePage);
    //this.navCtrl.push(AttendancePage);
  }

  viewAttendance(){
    const root = this.app.getRootNav();
    root.push(AttendanceSummaryPage);
  }

  goto_quiz(){
    const root = this.app.getRootNav();
    root.push(QuizListPage);
  }

  loadDashboard() {
    const root = this.app.getRootNav();
    root.push(DashboardPage);
  }

  new_visit(){
    const root = this.app.getRootNav();
    root.push(VisitStorePage);

  }

  // If the user enters text in the support question and then navigates
  // without submitting first, ask if they meant to leave the page
  ionViewCanLeave(): boolean | Promise<boolean> {
    // If the support message is empty we should just navigate
    return true;
  }
}
