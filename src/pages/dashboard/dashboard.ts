import { Component, ViewChild } from '@angular/core';
import { App, ActionSheet, ActionSheetController, Config, NavController } from 'ionic-angular';
import { ProgressBarComponent } from '../../components/progress-bar/progress-bar';
import { Chart } from 'chart.js';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-user',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  actionSheet: ActionSheet;
  @ViewChild('lineCanvas') lineCanvas: any;

  lineChart: any;

  constructor(public actionSheetCtrl: ActionSheetController,
  public app: App,
  public navCtrl: NavController, public config: Config) {}

  ionViewDidLoad() {
  this.lineChart = new Chart(this.lineCanvas.nativeElement, {

          type: 'line',
          data: {
              labels: ["January", "February", "March", "April", "May", "June", "July"],
              datasets: [
                  {
                      label: "My First dataset",
                      fill: false,
                      lineTension: 0.1,
                      backgroundColor: "rgba(75,192,192,0.4)",
                      borderColor: "rgba(75,192,192,1)",
                      borderCapStyle: 'butt',
                      borderDash: [],
                      borderDashOffset: 0.0,
                      borderJoinStyle: 'miter',
                      pointBorderColor: "rgba(75,192,192,1)",
                      pointBackgroundColor: "#fff",
                      pointBorderWidth: 1,
                      pointHoverRadius: 5,
                      pointHoverBackgroundColor: "rgba(75,192,192,1)",
                      pointHoverBorderColor: "rgba(220,220,220,1)",
                      pointHoverBorderWidth: 2,
                      pointRadius: 1,
                      pointHitRadius: 10,
                      data: [65, 59, 80, 81, 56, 55, 40],
                      spanGaps: false,
                  }
              ]
          }

      });
  }
  gotohome(){
    const root = this.app.getRootNav();
    root.push(TabsPage, { tabIndex: 1 });
  }
}
