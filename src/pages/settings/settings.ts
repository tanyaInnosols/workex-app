import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';

import { PopoverPage } from '../about-popover/about-popover';
import { UserData } from '../../providers/user-data';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public app: App, public navCtrl: NavController, public popoverCtrl: PopoverController, public userData: UserData) { }

  presentPopover(event: Event) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({ ev: event });
  }

  logout(){
    //let nav = this.app.getComponent('nav');
    setTimeout(() => {
      this.userData.logout();
      const root = this.app.getRootNav();
      root.setRoot(LoginPage);
    }, 1000);
  }

}
