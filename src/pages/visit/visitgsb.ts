import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';
import { VisitVisibilityPage } from '../visit/visitvisibility'

@Component({
  selector: 'page-user',
  templateUrl: 'gsb.html'
})
export class VisitGsbPage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  next(){
    const root = this.app.getRootNav();
    root.push(VisitVisibilityPage);
  }

}
