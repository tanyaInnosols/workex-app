import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';
import { VisitGsbPage } from '../visit/visitgsb'
import { VisitCompetitionPage } from '../visit/visitcompetition'
import { VisitStockPage } from '../visit/visitstock'
import { VisitVisibilityPage } from '../visit/visitvisibility'

@Component({
  selector: 'page-user',
  templateUrl: 'form-list.html'
})
export class VisitOptionsPage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  opengsb(){
    const root = this.app.getRootNav();
    root.push(VisitGsbPage);
  }
  openstock(){
    const root = this.app.getRootNav();
    root.push(VisitStockPage);
  }
  openvisibility(){
    const root = this.app.getRootNav();
    root.push(VisitVisibilityPage);
  }
  opencompetition(){
    const root = this.app.getRootNav();
    root.push(VisitCompetitionPage);
  }

}
