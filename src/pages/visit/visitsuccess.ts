import { Component } from '@angular/core';
import { TabsPage } from '../tabs/tabs';
import { App, PopoverController, NavController } from 'ionic-angular';

@Component({
  selector: 'page-user',
  templateUrl: 'success.html'
})
export class VisitSuccessPage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  gotohome(){
    const root = this.app.getRootNav();
    root.push(TabsPage, { tabIndex: 1 });
  }

}
