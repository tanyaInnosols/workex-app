import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';
import { VisitSuccessPage } from '../visit/visitsuccess'

@Component({
  selector: 'page-user',
  templateUrl: 'submit.html'
})
export class VisitSubmitPage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  next(){
    const root = this.app.getRootNav();
    root.push(VisitSuccessPage);
  }

}
