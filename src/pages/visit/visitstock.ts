import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';
import { VisitSubmitPage } from '../visit/visitsubmit'

@Component({
  selector: 'page-user',
  templateUrl: 'stock.html'
})
export class VisitStockPage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  next(){
    const root = this.app.getRootNav();
    root.push(VisitSubmitPage);
  }

}
