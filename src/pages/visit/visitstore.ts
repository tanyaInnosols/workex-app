import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';
import { VisitOptionsPage } from '../visit/visitoptions'

@Component({
  selector: 'page-user',
  templateUrl: 'select-store.html'
})
export class VisitStorePage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  visitformlist(){
    const root = this.app.getRootNav();
    root.push(VisitOptionsPage);
  }

}
