import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';
import { VisitStockPage } from '../visit/visitstock'

@Component({
  selector: 'page-user',
  templateUrl: 'competition.html'
})
export class VisitCompetitionPage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  next(){
    const root = this.app.getRootNav();
    root.push(VisitStockPage);
  }
}
