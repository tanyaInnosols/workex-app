import { Component } from '@angular/core';

import { App, PopoverController, NavController } from 'ionic-angular';
import { VisitCompetitionPage } from '../visit/visitcompetition'

@Component({
  selector: 'page-user',
  templateUrl: 'visibility.html'
})
export class VisitVisibilityPage {

  constructor(public app: App,
  public navCtrl: NavController) { }
  next(){
    const root = this.app.getRootNav();
    root.push(VisitCompetitionPage);
  }

}
